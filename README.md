# Tcc

This project was implemented for academic purposes.
The objective of this is to seek data, synchrony and asynchronous, from external sources on the Covid-19 pandemic in Brazil and to compare the processing time.

### Prerequisites

What things you need to install the software and how to install them
- [Golang](http://golang.org/)(>13.0)

### Running in terminal

go run main.go
