package covid

import (
	"encoding/json"
	"net/http"
	"time"
)

type CoronaLmaoNinjaApi struct {
	responseData struct{
		Updated int64 `json:"updated"`
		Cases int `json:"cases"`
		Deaths int `json:"deaths"`
	}
}

func ProvideCoronaLmaoNinjaApi() *CoronaLmaoNinjaApi  {
	return &CoronaLmaoNinjaApi{}
}

func (c *CoronaLmaoNinjaApi) GetData() *CovidData {
	init := time.Now()
	res, err := http.Get("https://corona.lmao.ninja/v2/countries/brazil")
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&c.responseData)
	if err != nil {
		panic(err)
	}
	end := time.Now()
	a := time.Unix(0, c.responseData.Updated * int64(time.Millisecond))
	return ProvideCovidData(&a, c.responseData.Cases, c.responseData.Deaths, &init, &end, "corona_limao_ninja")
}