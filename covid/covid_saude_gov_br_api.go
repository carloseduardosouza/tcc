package covid

import (
	"encoding/json"
	"net/http"
	"time"
)

type CovidSaudeGovBrApi struct {
	responseData struct{
		Confirmados struct{
			Total int `json:"total,string"`
		}
		Obitos struct{
			Total int `json:"total,string"`
		}
		Data time.Time `json:"dt_updated"`
	}
}

func ProvideCovidSaudeGovBrApi() *CovidSaudeGovBrApi  {
	return &CovidSaudeGovBrApi{}
}

func (c *CovidSaudeGovBrApi) GetData() *CovidData {
	init := time.Now()
	res, err := http.Get("https://xx9p7hp1p7.execute-api.us-east-1.amazonaws.com/prod/PortalGeralApi")
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&c.responseData)
	if err != nil {
		panic(err)
	}
	end := time.Now()
	return ProvideCovidData(&c.responseData.Data, c.responseData.Confirmados.Total, c.responseData.Obitos.Total, &init, &end, "covid_saude_gov_br")
}