package covid

import (
	"encoding/json"
	"net/http"
	"time"
)

type Covid19ApiOrg struct {
	responseData struct{
		Cases int
		Deaths int
		Updated string `json:"last_update"`
	}
}

func ProvideCovid19ApiOrg() *Covid19ApiOrg  {
	return &Covid19ApiOrg{}
}

func (c *Covid19ApiOrg) GetData() *CovidData {
	init := time.Now()
	res, err := http.Get("https://covid19-api.org/api/status/br")
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&c.responseData)
	if err != nil {
		panic(err)
	}
	end := time.Now()
	t, err := time.Parse("2006-01-02T15:04:05", c.responseData.Updated)
	if err != nil {
		panic(err)
	}
	return ProvideCovidData(&t, c.responseData.Cases, c.responseData.Deaths, &init, &end, "covid19_api_org")
}