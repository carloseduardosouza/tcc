package covid

import "time"

type CovidData struct {
	date *time.Time
	confirmed int
	deaths int
	init *time.Time
	end *time.Time
	integrationName string
}

func ProvideCovidData(date *time.Time, confirmed int, deaths int, init *time.Time, end *time.Time, integrationName string) *CovidData  {
	return &CovidData{date, confirmed, deaths, init, end, integrationName}
}

func (cd *CovidData) GetDate() *time.Time  {
	return cd.date
}

func (cd *CovidData) GetConfirmed() int  {
	return cd.confirmed
}

func (cd *CovidData) GetDeaths() int  {
	return cd.deaths
}

func (cd *CovidData) GetInit() *time.Time  {
	return cd.init
}

func (cd *CovidData) GetEnd() *time.Time  {
	return cd.end
}

func (cd *CovidData) GetName() string  {
	return cd.integrationName
}

func (cd *CovidData) GetDuration() *time.Duration {
	init := cd.init
	duration := cd.end.Sub(*init)
	return &duration
}