package covid

type CovidProvider interface {
	GetData() *CovidData
}