package covid

import (
	"encoding/json"
	"net/http"
	"time"
)

type Covid19BrazilApi struct {
	responseData struct{
		Data struct{
			Confirmed int
			Deaths int
			Updated_at time.Time
		}
	}
}

func ProvideCovid19BrazilApi() *Covid19BrazilApi  {
	return &Covid19BrazilApi{}
}

func (c *Covid19BrazilApi) GetData() *CovidData {
	init := time.Now()
	res, err := http.Get("https://covid19-brazil-api.now.sh/api/report/v1/brazil")
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&c.responseData)
	if err != nil {
		panic(err)
	}
	end := time.Now()
	return ProvideCovidData(&c.responseData.Data.Updated_at, c.responseData.Data.Confirmed, c.responseData.Data.Deaths, &init, &end, "covid19_brazil")
}