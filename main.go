package main

import (
	"fmt"
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"log"
	"strconv"
	"tcc/covid"
	"time"
)


type ProviderList []covid.CovidProvider
type CovidDataList map[int]*covid.CovidData


func (list ProviderList) asyncProcess() CovidDataList {
	chanTemp := make(chan covid.CovidData)

	for _, p := range list {
		go func(c covid.CovidProvider) {
			data := c.GetData()
			chanTemp <- *data
		}(p)
	}

	result := make(map[int]*covid.CovidData)

	for i := 0; i < len(list); i++ {
		temp := <-chanTemp
		result[i] = &temp
	}

	return result
}

func (list ProviderList) syncProcess() CovidDataList  {
	result := make(map[int]*covid.CovidData)
	i := 0
	for _, p := range list {
		func(c covid.CovidProvider) {
			result[i] = c.GetData()
			i++
		}(p)
	}
	return result
}

func main()  {
	if err := ui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v", err)
	}
	defer ui.Close()

	list := ProviderList{
		covid.ProvideCovid19BrazilApi(),
		covid.ProvideCoronaLmaoNinjaApi(),
		covid.ProvideCovidSaudeGovBrApi(),
		covid.ProvideCovid19ApiOrg(),
	}
	list.asyncProcess()
	t1 := time.Now()
	result1 := list.syncProcess()
	t2 := time.Now()
	d1 := t2.Sub(t1)
	printListResult(result1, d1, "sync", 4, 1, 5, 16)

	t3 := time.Now()
	result2 := list.asyncProcess()
	t4 := time.Now()
	d2 := t4.Sub(t3)
	printListResult(result2, d2, "async", 20, 17, 21, 32)
	uiEvents := ui.PollEvents()
	for {
		e := <-uiEvents
		switch e.ID {
		case "q", "<C-c>":
			return
		}
	}
}

func printListResult(result CovidDataList, d time.Duration, mode string, y11 int, y21 int, y12 int, y22 int)  {

	head := widgets.NewTable()
	head.Rows = [][]string{
		[]string{fmt.Sprintf("Mode: %s", mode), fmt.Sprintf("Duration: %f seconds", d.Seconds())},
	}
	head.TextStyle = ui.NewStyle(ui.ColorWhite)
	head.RowSeparator = true
	head.SetRect(0, y11, 70, y21)
	head.RowStyles[0] = ui.NewStyle(ui.ColorWhite, ui.ColorBlack, ui.ModifierBold)
	head.FillRow = true

	table := widgets.NewTable()
	var a = make([][]string, 6)
	a[0] = []string{"Integration name", "Cases", "deaths", "Updated at", "Init Provider", "End Provider", "Duration"}

	for i := range result{
		idx := i + 1
		a[idx] = []string{
			result[i].GetName(),
			strconv.FormatInt(int64(result[i].GetConfirmed()), 10),
			strconv.FormatInt(int64(result[i].GetDeaths()), 10),
			result[i].GetDate().Format("02-Jan-2006 15:04:05"),
			result[i].GetInit().Format("15:04:05.000000"),
			result[i].GetEnd().Format("15:04:05.000000"),
			fmt.Sprintf("%f", result[i].GetDuration().Seconds()),
		}
	}

	table.Rows = a
	table.TextStyle = ui.NewStyle(ui.ColorWhite)
	table.RowSeparator = true
	table.BorderStyle = ui.NewStyle(ui.ColorGreen)
	table.SetRect(0, y12, 150, y22)
	table.FillRow = true
	table.RowStyles[0] = ui.NewStyle(ui.ColorWhite, ui.ColorBlack, ui.ModifierBold)
	ui.Render(head, table)
}